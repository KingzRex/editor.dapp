import React from 'react'
import RouteTools from '../lib/RouteTools'
import { withFile } from '../file'
import { FiFolder } from 'react-icons/fi'

function GoToDir( props ) {
  function action() {
    const { api } = RouteTools.parse().addresses
    const { dirname } = props.withFile

    const sufixUrl = api + dirname

    props.withFile.forceSave()
    RouteTools._go( `https://mfs.octoz.app/#${ sufixUrl }` )
  }

  return (
    <button title="back to mfs" onClick={ action }>
      <FiFolder />
    </button>
  )
}

export default withFile( GoToDir )
