import React, { createContext, Component } from 'react'

const StatusContext = createContext()

class Status extends Component {
  static ALERT = 'ALERT'
  static REFRESH = 'REFRESH'
  static SAVED = 'SAVED'

  state = {
    status: undefined
  }

  update( newStatus ) {
    this.setState( { status: newStatus } )
  }

  render() {
    return (
      <StatusContext.Provider value={ {
        status: this.state.status,
        update: this.update.bind( this )
      } }>
        { this.props.children }
      </StatusContext.Provider>
    )
  }
}

const withStatus = ( ComponentAlias ) => {
  return props => (
    <StatusContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withStatus={ context } />
      } }
    </StatusContext.Consumer>
  )
}

export default Status

export {
  StatusContext,
  withStatus
}
