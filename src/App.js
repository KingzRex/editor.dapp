import React from 'react'
import './skin/index.sass'
import Ipfs from './ipfs'
import NoIpfs from './noIpfs'
import Status from './status'
import File from './file'
import TopBar from './topBar'
import Editor from './editor'
import BottomBar from './bottomBar'
import { homepage, bugs } from '../package.json'

function App() {
  return (
    <Ipfs noIpfs={ NoIpfs } links={ { homepage, issues: bugs.url } } >
      <Status>
        <File>
          <TopBar />
          <Editor />
          <BottomBar />
        </File>
      </Status>
    </Ipfs>
  )
}

export default App
