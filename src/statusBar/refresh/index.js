import React from 'react'

import { FiRefreshCw } from 'react-icons/fi'

function Refresh( props ) {

  return (
    <div className="status-bar">
      <button className="spin">
        <FiRefreshCw />
      </button>
    </div>
  )
}

export default Refresh
