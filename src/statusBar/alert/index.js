import React from 'react'

import { FiAlertTriangle } from 'react-icons/fi'

function Alert( props ) {

  return (
    <div className="status-bar">
      <button title="automatic save is only available when you assign a filename.">
        <FiAlertTriangle />
      </button>
    </div>
  )
}

export default Alert
