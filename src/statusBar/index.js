import React, { Component } from 'react'
import './StatusBar.sass'
import Status, { withStatus } from '../status'
import { withFile } from '../file'

import Refresh from './refresh'
import Saved from './saved'
import Alert from './alert'

class StatusBar extends Component {

  componentDidMount() {
    setTimeout( () => {
      if ( ! this.props.withFile.filename )
        this.props.withStatus.update( Status.ALERT )
    }, 700 )
  }

  render() {
    const { status } = this.props.withStatus

    switch( status ) {
      case Status.ALERT: return <Alert />
      case Status.REFRESH: return <Refresh />
      case Status.SAVED: return <Saved />
      default: return <></>
    }
  }
}

export default withStatus( withFile( StatusBar ) )
