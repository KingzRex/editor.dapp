import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import { IoCloudDoneOutline } from 'react-icons/io5'

function Saved( props ) {
  const [ showMessage, setShowMessage ] = useState( true )
  const { messageTimeout = 1400 } = props

  useEffect( () => {
    if ( !showMessage ) return
    setTimeout( () => {
      setShowMessage( false )
    }, messageTimeout )
  } )

  return (
    <div className="status-bar">
      <button>
        <IoCloudDoneOutline />
      </button>
      { showMessage ? <label>saved!</label> : null }
    </div>
  )
}

Saved.propTypes = {
  messageTimeout: PropTypes.number
}

export default Saved
